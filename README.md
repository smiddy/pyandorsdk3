# pyAndorSDK3

Python API for the Andor SDK 3

## Class Andorbase
Class as ctypes wrapper for Andor SDK from AT and ATUTILITY. All functions can be called from the base class. See `initialisation` and `singleacquisition` as examples.
### Data types
Data types are implemented in the base class for initialization of variables.

#### String AT_WC
For strings of types `AT_WC` there are to possibilities. Immutable strings can simply be called with `AT_WCPtr`, e.g.

```python
immutablestring = Andorbase.AT_WCPtr('PixelEncoding')
````

For mutable string a unicode buffer has to be initialized and cast to the appropriate pointer and handed over to the function

```python
pixelencodingmeta = ctypes.create_unicode_buffer(30)
pixelencodingmetaptr = ctypes.cast(pixelencodingmeta, ctypes.POINTER(Andorbase.AT_WC))
```

#### Buffer
Image buffers must be create as string buffer and then cast accordingly

```python
buffer = ctypes.create_string_buffer(imagesizebytes.value)  # imagesizebytes from preceding funtion call
bufferU8 = ctypes.cast(buffer, ctypes.POINTER(Andorbase.AT_U8))
bufferptr = ctypes.cast(bufferU8, ctypes.POINTER(Andorbase.AT_U8))
```

## Class Istarscmos
A pythonic wrapper for the iStar sCMOS camera without the need of knowledge of `ctypes`. An example is present in `istar.py`.

All features can be accessed with the two methods

```python
value = Istarscmos.getfeature(feature)
Istarscmos.setfeature(feature, value)
```
All values are saved in the dict `_featureval`. This is only supposed as an overview, changes in the dict will not be transmitted to the camera. In addition, the class method `getallfeatures` will query all valid features from camera.

### Buffer Management
Buffer management is handled with class `Istarbuffer`. A new instance is initiated during operation with `Istarscmos.createbuffer()`. Buffer manangement is realized with methods

```python
Istarscmos.queuebuffer(buffer)
Istarscmos.waitbuffer(buffer)
numpyarray, metadata = Istarscmos.convertbuffer(buffer)
```

### Class Methods
Various methods are available for direct use in python as an wrapper for the ctypes functions of `Andorbase`:
```python
Istarscmos.command(command)
Istarscmos.flush()
Istarscmos.isimplemented(feature)
Istarscmos.isreadable(feature)
Istarscmos.iswriteable(feature)
Istarscmos.isreadonly()
```