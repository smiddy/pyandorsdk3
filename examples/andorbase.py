from pyandorsdk3 import Andorbase
import ctypes
import logging


logging.basicConfig(filename='example.log', level=logging.DEBUG)
camnbr = 0  # last two indices for SimCam
baseCam = Andorbase()
camhndl = baseCam.AT_H()
baseCam.AT_InitialiseLibrary()
baseCam.AT_Open(camnbr, ctypes.byref(camhndl))
# Feature Controls
feat = baseCam.AT_WCPtr('AcquisitionStart')
impl = baseCam.AT_BOOL()
readabl = baseCam.AT_BOOL()
writabl = baseCam.AT_BOOL()
rdonly = baseCam.AT_BOOL()
baseCam.AT_IsImplemented(camhndl, feat, ctypes.byref(impl))
baseCam.AT_IsReadable(camhndl, feat, readabl)
baseCam.AT_IsWritable(camhndl, feat, writabl)
baseCam.AT_IsReadOnly(camhndl, feat, rdonly)
print("Writeable:", writabl.value)
print("Readable:", rdonly.value)
# Integer Controls
frameCountSet = baseCam.AT_64(2)
frameCountGet = baseCam.AT_64()
frameCountMax = baseCam.AT_64()
frameCountMin = baseCam.AT_64()
featInt = baseCam.AT_WCPtr("FrameCount")
baseCam.AT_SetInt(camhndl, featInt, frameCountSet)
baseCam.AT_GetInt(camhndl, featInt, ctypes.byref(frameCountGet))
baseCam.AT_GetIntMax(camhndl, featInt, ctypes.byref(frameCountMax))
print("Current frame count:", frameCountGet.value)
print("Frame count max:", frameCountMax.value)
print("Frame count min:", frameCountMin.value)
# Floating Point Controls
featfloat = baseCam.AT_WCPtr("ExposureTime")
exptimeset = ctypes.c_double(0.1)
exptimeget = ctypes.c_double()
exptimemax = ctypes.c_double()
exptimemin = ctypes.c_double()
baseCam.AT_SetFloat(camhndl, featfloat, exptimeset)
baseCam.AT_GetFloat(camhndl, featfloat, exptimeget)
baseCam.AT_GetFloatMax(camhndl, featfloat, exptimemax)
baseCam.AT_GetFloatMin(camhndl, featfloat, exptimemin)
print("Current exptime:", exptimeget.value)
print("Max exptime:", exptimemax.value)
print("Min exptime:", exptimemin.value)
# Bool controls
boolfeat = baseCam.AT_WCPtr("SensorCooling")
coolset = baseCam.AT_BOOL(1)
coolget = baseCam.AT_BOOL(22)
baseCam.AT_SetBool(camhndl, boolfeat, coolset)
baseCam.AT_GetBool(camhndl, boolfeat, coolget)
print("SensorCooling:", bool(coolget.value))
# Enumerated controls
enumfeat = baseCam.AT_WCPtr("TriggerMode")
triggsetindex = ctypes.c_int(0)
triggsetstring = baseCam.AT_WCPtr("Internal")
trigggetindex = ctypes.c_int(22)
trigggetcount = ctypes.c_int(0)
triggetenumstring = ctypes.create_unicode_buffer(30)
triggetenumstringptr = ctypes.cast(triggetenumstring, baseCam.AT_WCPtr)
triggavail = baseCam.AT_BOOL()
triggimpl = baseCam.AT_BOOL()
baseCam.AT_SetEnumIndex(camhndl, enumfeat, triggsetindex)
baseCam.AT_SetEnumString(camhndl, enumfeat, triggsetstring)
baseCam.AT_GetEnumIndex(camhndl, enumfeat, trigggetindex)
baseCam.AT_GetEnumCount(camhndl, enumfeat, trigggetcount)
baseCam.AT_GetEnumStringByIndex(camhndl, enumfeat, triggsetindex, triggetenumstringptr, 30)
baseCam.AT_IsEnumIndexAvailable(camhndl, enumfeat, triggsetindex, triggavail)
baseCam.AT_IsEnumIndexImplemented(camhndl, enumfeat, triggsetindex, triggimpl)
print("TriggerMode index:", trigggetindex.value)
print("TriggerMode count:", trigggetcount.value)
print("TriggerMode descriptor:", triggetenumstring.value)
print("TriggerMode available:", bool(triggavail.value))
print("TriggerMode implemented:", bool(triggimpl.value))
# String controls
strfeat = baseCam.AT_WCPtr('CameraModel')
modelname = ctypes.create_unicode_buffer(30)
modelnameptr = ctypes.cast(modelname, ctypes.POINTER(baseCam.AT_WC))
strfeatlenght = ctypes.c_int()
baseCam.AT_GetString(camhndl, strfeat, modelnameptr, 30)
baseCam.AT_GetStringMaxLength(camhndl, strfeat, strfeatlenght)
print("CameraModel:", modelname.value)
print("CameraModel max string length:", strfeatlenght.value)
# Buffer management
imagesizebytes = baseCam.AT_64()
baseCam.AT_GetInt(camhndl, baseCam.AT_WCPtr("ImageSizeBytes"), ctypes.byref(imagesizebytes))
buffer = ctypes.create_string_buffer(imagesizebytes.value)
bufferU8 = ctypes.cast(buffer, ctypes.POINTER(baseCam.AT_U8))
bufferptr = ctypes.cast(bufferU8, ctypes.POINTER(baseCam.AT_U8))
baseCam.AT_QueueBuffer(camhndl, bufferU8, ctypes.c_int(imagesizebytes.value))
# Command controls
baseCam.AT_Command(camhndl, baseCam.AT_WCPtr("AcquisitionStart"))
baseCam.AT_WaitBuffer(camhndl, ctypes.byref(bufferU8), ctypes.c_int(imagesizebytes.value), ctypes.c_uint(1000))
baseCam.AT_Command(camhndl, baseCam.AT_WCPtr("AcquisitionStop"))
baseCam.AT_Flush(camhndl)
baseCam.AT_Close(camhndl)
baseCam.AT_FinaliseLibrary()
