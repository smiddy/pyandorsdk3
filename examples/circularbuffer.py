from pyandorsdk3 import Istarscmos
import logging
import numpy as np

# Number of images to acquire
noimages = 100
nobuffers = 10

# Create logger
logging.basicConfig(level=logging.DEBUG, filename='example.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

with Istarscmos() as cam:
    cam.flush()
    # Set camera
    cam.setfeature("CycleMode", "Continuous")
    print("CycleMode:", cam.getfeature("CycleMode"))
    print("FrameCount:", cam.getfeature("FrameCount"))
    print("AccumulateCount:", cam.getfeature("AccumulateCount"))
    print("FrameRate:", cam.getfeature("FrameRate"))
    # Allocate memory
    buffers = [cam.createbuffer() for ii in range(nobuffers)]
    [cam.queuebuffer(buffer) for buffer in buffers]
    images = np.zeros((noimages, buffers[0].metadata['height'], buffers[0].metadata['width']), dtype='uint16')
    # Acquisition
    print("Starting acquisition")
    cam.command('AcquisitionStart')
    for ii in range(noimages):
        jj = ii % nobuffers
        cam.waitbuffer(buffers[jj])
        images[ii, :, :], _ = cam.convertbuffer(buffers[jj])
        cam.queuebuffer(buffers[jj])
    cam.command('AcquisitionStop')
    print("Acquisition finished")
    pass
