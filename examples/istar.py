from pyandorsdk3 import Istarscmos
import logging

# User input for stabilised cooling
cooling = True

logging.basicConfig(filename='example.log', level=logging.DEBUG)
with Istarscmos() as cam:
    cam.setfeature('LogLevel', 'DEBUG')
    print(cam.getfeature('AccumulateCount'))
    cam.setfeature('AccumulateCount', 1)
    print('Minimal AccumulateCount:', cam.getfeaturemin('AccumulateCount'))
    print('Maximal AccumulateCount:', cam.getfeaturemax('AccumulateCount'))
    print(cam.getfeature('AOIBinning'))
    cam.setfeature('AOIBinning', '1x1')
    print(cam.getfeature('DDGIOCEnable'))
    cam.setfeature('DDGIOCEnable', True)
    print(cam.getfeature('ExposureTime'))
    print('Minimal Exposure:', cam.getfeaturemin('ExposureTime'))
    print('Maximal Exposure:', cam.getfeaturemax('ExposureTime'))
    cam.setfeature('ExposureTime', 0.1)
    print(cam.getfeature('CameraModel'))
    cam.setfeature('MetadataEnable', False)
    print(cam.getfeature('MetadataEnable'))
    # By default, cooling is off. The cooling has to be switched on manually
    if cooling:
        cam.stabilizecooling()
    print(cam.isimplemented('AccumulateCount'))
    # Acquisition
    buf1 = cam.createbuffer()
    cam.queuebuffer(buf1)
    cam.command('AcquisitionStart')
    cam.waitbuffer(buf1)
    cam.command('AcquisitionStop')
    img1, metadata = cam.convertbuffer(buf1)
    if cooling:
        cam.keepcool()
pass
