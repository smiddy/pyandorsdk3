from pyandorsdk3 import Istarscmos
import logging
import numpy as np

# Number of images to acquire
noimages = 10

# Create logger
logging.basicConfig(level=logging.DEBUG, filename='example.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

with Istarscmos() as cam:
    cam.flush()
    buffers = [cam.createbuffer() for ii in range(noimages)]
    [cam.queuebuffer(buffer) for buffer in buffers]
    cam.setfeature("CycleMode", "Fixed")
    cam.setfeature("FrameCount", noimages)
    print("CycleMode:", cam.getfeature("CycleMode"))
    print("FrameCount:", cam.getfeature("FrameCount"))
    print("AccumulateCount:", cam.getfeature("AccumulateCount"))
    print("FrameRate:", cam.getfeature("FrameRate"))
    # Acquisition
    print("Starting acquisition")
    cam.command('AcquisitionStart')
    [cam.waitbuffer(buffer) for buffer in buffers]
    cam.command('AcquisitionStop')
    print("Acquisition finished")
    # Convert to numpy image
    images = np.zeros((noimages, buffers[0].metadata['height'], buffers[0].metadata['width']), dtype='uint16')
    for ii in range(noimages):
        images[ii, :, :], _ = cam.convertbuffer(buffers[ii])
    pass
