from pyandorsdk3 import Andorbase
import ctypes
import logging
import numpy as np

logging.basicConfig(filename='example.log', level=logging.DEBUG)
camnbr = 0  # last two indices for SimCam
baseCam = Andorbase()
camhndl = baseCam.AT_H()
baseCam.AT_InitialiseLibrary()
baseCam.AT_Open(camnbr, ctypes.byref(camhndl))
# Enable metadata
baseCam.AT_SetBool(camhndl, baseCam.AT_WCPtr("MetadataEnable"), baseCam.AT_BOOL(1))
# Get information for acquisition
width = baseCam.AT_64()
height = baseCam.AT_64()
stride = baseCam.AT_64()
imagesizebytes = baseCam.AT_64()
pixelencodingindex = ctypes.c_int()
pixelencoding = baseCam.AT_WCPtr(30 * 'b')
baseCam.AT_GetInt(camhndl, baseCam.AT_WCPtr('AOIWidth'), ctypes.byref(width))
baseCam.AT_GetInt(camhndl, baseCam.AT_WCPtr('AOIHeight'), ctypes.byref(height))
baseCam.AT_GetInt(camhndl, baseCam.AT_WCPtr('AOIStride'), ctypes.byref(stride))
baseCam.AT_GetInt(camhndl, baseCam.AT_WCPtr("ImageSizeBytes"), ctypes.byref(imagesizebytes))
baseCam.AT_GetEnumIndex(camhndl, baseCam.AT_WCPtr('PixelEncoding'), pixelencodingindex)
baseCam.AT_GetEnumStringByIndex(camhndl, baseCam.AT_WCPtr('PixelEncoding'), pixelencodingindex, pixelencoding, 30)
# Initialize buffer
buffer = ctypes.create_string_buffer(imagesizebytes.value)
bufferU8 = ctypes.cast(buffer, ctypes.POINTER(baseCam.AT_U8))
bufferptr = ctypes.cast(bufferU8, ctypes.POINTER(baseCam.AT_U8))
baseCam.AT_QueueBuffer(camhndl, bufferU8, ctypes.c_int(imagesizebytes.value))
# Acquisition
baseCam.AT_Command(camhndl, baseCam.AT_WCPtr("AcquisitionStart"))
baseCam.AT_WaitBuffer(camhndl, bufferptr, ctypes.c_int(imagesizebytes.value), ctypes.c_uint(1000))
baseCam.AT_Command(camhndl, baseCam.AT_WCPtr("AcquisitionStop"))
# Close camera
baseCam.AT_Flush(camhndl)
baseCam.AT_Close(camhndl)
baseCam.AT_FinaliseLibrary()
# Use utility library for image processing
img = ctypes.create_string_buffer(height.value * width.value * ctypes.sizeof(ctypes.c_uint16))
imgU8 = ctypes.cast(img, ctypes.POINTER(baseCam.AT_U8))
imgmeta = ctypes.create_string_buffer(height.value * width.value * ctypes.sizeof(ctypes.c_uint16))
imgmetaU8 = ctypes.cast(imgmeta, ctypes.POINTER(baseCam.AT_U8))
baseCam.AT_InitialiseUtilityLibrary()
baseCam.AT_ConvertBuffer(bufferU8, imgU8, width, height, stride, pixelencoding, baseCam.AT_WCPtr("Mono16"))
baseCam.AT_ConvertBufferUsingMetadata(bufferU8, imgmetaU8, imagesizebytes, baseCam.AT_WCPtr("Mono16"))
widthmeta = baseCam.AT_64()
heightmeta = baseCam.AT_64()
stridemeta = baseCam.AT_64()
timestamp = baseCam.AT_64()
pixelencodingmeta = ctypes.create_unicode_buffer(30)
pixelencodingmetaptr = ctypes.cast(pixelencodingmeta, ctypes.POINTER(baseCam.AT_WC))
pixelencodingmetasize = baseCam.AT_64(ctypes.sizeof(pixelencodingmeta))
baseCam.AT_GetWidthFromMetadata(bufferU8, imagesizebytes, widthmeta)
baseCam.AT_GetHeightFromMetadata(bufferU8, imagesizebytes, heightmeta)
baseCam.AT_GetStrideFromMetadata(bufferU8, imagesizebytes, stridemeta)
baseCam.AT_GetTimeStampFromMetadata(bufferU8, imagesizebytes, timestamp)
baseCam.AT_GetPixelEncodingFromMetadata(bufferU8, imagesizebytes, pixelencodingmetaptr, pixelencodingmetasize)
print("metadata width:", widthmeta.value)
print("metadata height:", heightmeta.value)
print("metadata stride:", stridemeta.value)
print("metadata timestamp:", timestamp.value)
print("metadata pixelencoding:", pixelencodingmeta.value)
baseCam.AT_FinaliseUtilityLibrary()
# Numpy conversion
imgnp = np.frombuffer(img, dtype=np.uint16)
imgnp = np.reshape(imgnp, (height.value, width.value))
imgmetanp = np.frombuffer(img, dtype=np.uint16)
imgmetanp = np.reshape(imgmetanp, (height.value, width.value))
print("Arrays equal:", np.array_equal(imgnp, imgmetanp))
pass
