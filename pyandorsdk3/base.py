import ctypes
import sys
import os
from pyandorsdk3.errcodes import errDict, errdDictut
import logging
from pyandorsdk3.version import __version__

# Define install location of the SDK
sdkLoc = r'C:\Program Files\Andor SDK3'

# Include the SDK folder
sys.path.append(sdkLoc)

# atdevsimcam only loaded for the use of simulated cameras
andorSimCam = ctypes.CDLL(os.path.join(sdkLoc, 'atdevsimcam.dll'))
andorlibusb = ctypes.CDLL(os.path.join(sdkLoc, 'atusb_libusb.dll'))
andorlibusb10 = ctypes.CDLL(os.path.join(sdkLoc, 'atusb_libusb10.dll'))
andorApogee = ctypes.CDLL(os.path.join(sdkLoc, 'atdevapogee.dll'))
andorRegCam = ctypes.CDLL(os.path.join(sdkLoc, 'atdevregcam.dll'))
andorDll = ctypes.CDLL(os.path.join(sdkLoc, 'atcore.dll'))
andorUtility = ctypes.CDLL(os.path.join(sdkLoc, 'atutility.dll'))

# create logger
logger = logging.getLogger(__name__)


class AndorSDK3Error(Exception):
    """Andor SDK3 Exception handle
    """

    def __init__(self, err):
        super().__init__()
        self.logger = logging.getLogger(__name__)
        try:
            self.errStr = 'Error {}: '.format(err) + errDict[err]
        except KeyError:
            self.errStr = 'Error {}: Unknown error number'.format(err)
        self.logger.error(self.errStr)

    def __str__(self):
        return self.errStr


class AndorSDK3UtError(Exception):
    """Andor SDK3 Exception handle
    """

    def __init__(self, err):
        super().__init__()
        self.logger = logging.getLogger(__name__)
        try:
            self.errStr = 'Error {}: '.format(err) + errdDictut[err]
        except KeyError:
            self.errStr = 'Error {}: Unknown error number'.format(err)
        self.logger.error(self.errStr)

    def __str__(self):
        return self.errStr


class Andorbase(object):
    """ Python wrapper for the Andor SDK 3
    """

    def __init__(self):
        super().__init__()
        self.logger = logging.getLogger(__name__)
        # Andor datatypes
        self.AT_H = ctypes.c_int
        self.AT_WC = ctypes.c_wchar
        self.AT_WCPtr = ctypes.c_wchar_p
        self.AT_BOOL = ctypes.c_int
        self.AT_64 = ctypes.c_longlong
        self.AT_U8 = ctypes.c_ubyte
        self.AT_HANDLE_SYSTEM = ctypes.c_int(1)
        self.__version__ = __version__

    def _errcheck(self, errno):
        if errno != 0:
            raise AndorSDK3Error(errno)

    def _errcheckut(self, errno):
        if errno != 0:
            raise AndorSDK3UtError(errno)

    def AT_InitialiseLibrary(self):
        andorDll.AT_InitialiseLibrary.restype = ctypes.c_int
        self._errcheck(andorDll.AT_InitialiseLibrary())
        self.logger.debug('Library initialised')

    def AT_FinaliseLibrary(self):
        andorDll.AT_FinaliseLibrary.restype = ctypes.c_int
        self._errcheck(andorDll.AT_FinaliseLibrary())
        self.logger.debug('Library finalised')

    def AT_Open(self, deviceindex, handleptr):
        andorDll.AT_Open.argtypes = (ctypes.c_int, ctypes.POINTER(self.AT_H))
        andorDll.AT_Open.restype = ctypes.c_int
        self._errcheck(andorDll.AT_Open(deviceindex, handleptr))
        self.logger.debug('Device {}  with handle {} opened'.format(deviceindex, handleptr._obj.value))

    def AT_Close(self, handle):
        andorDll.AT_Close.argtypes = (self.AT_H,)
        andorDll.AT_Close.restype = ctypes.c_int
        self._errcheck(andorDll.AT_Close(handle))
        self.logger.debug('Device with handle {} closed'.format(handle.value))

    def AT_RegisterFeatureCallback(self):
        pass

    def AT_UnregisterFeatureCallback(self):
        pass

    def AT_IsImplemented(self, hndl, feature, implemented):
        andorDll.AT_IsImplemented.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_BOOL))
        andorDll.AT_IsImplemented.restype = ctypes.c_int
        self._errcheck(andorDll.AT_IsImplemented(hndl, feature, implemented))
        try:
            self.logger.debug('Feature {} implemented: {}'.format(feature.value, bool(implemented.value)))
        except AttributeError:
            self.logger.debug('Feature {} implemented: {}'.format(feature.value, bool(implemented._obj.value)))

    def AT_IsReadable(self, hndl, feature, readable):
        andorDll.AT_IsReadable.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_BOOL))
        andorDll.AT_IsReadable.restype = ctypes.c_int
        self._errcheck(andorDll.AT_IsReadable(hndl, feature, readable))
        try:
            self.logger.debug('Feature {} is readable: {}'.format(feature.value, bool(readable.value)))
        except AttributeError:
            self.logger.debug('Feature {} is readable: {}'.format(feature.value, bool(readable._obj.value)))

    def AT_IsWritable(self, hndl, feature, writable):
        andorDll.AT_IsWritable.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_BOOL))
        andorDll.AT_IsWritable.restype = ctypes.c_int
        self._errcheck(andorDll.AT_IsWritable(hndl, feature, writable))
        try:
            self.logger.debug('Feature {} is writable: {}'.format(feature.value, bool(writable.value)))
        except AttributeError:
            self.logger.debug('Feature {} is writable: {}'.format(feature.value, bool(writable._obj.value)))

    def AT_IsReadOnly(self, hndl, feature, readonly):
        andorDll.AT_IsReadOnly.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_BOOL))
        andorDll.AT_IsReadOnly.restype = ctypes.c_int
        self._errcheck(andorDll.AT_IsReadOnly(hndl, feature, readonly))
        try:
            self.logger.debug('Feature {} is read only: {}'.format(feature.value, bool(readonly.value)))
        except AttributeError:
            self.logger.debug('Feature {} is read only: {}'.format(feature.value, bool(readonly._obj.value)))

    def AT_SetInt(self, hndl, feature, intval):
        andorDll.AT_SetInt.argtypes = (self.AT_H, self.AT_WCPtr, self.AT_64)
        andorDll.AT_SetInt.restype = ctypes.c_int
        self._errcheck(andorDll.AT_SetInt(hndl, feature, intval))
        self.logger.debug('Feature {} is set to {}'.format(feature.value, intval.value))

    def AT_GetInt(self, hndl, feature, intvalPtr):
        andorDll.AT_GetInt.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_64))
        andorDll.AT_GetInt.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetInt(hndl, feature, intvalPtr))
        try:
            self.logger.debug('Feature {} has value {}'.format(feature.value, intvalPtr.value))
        except AttributeError:
            self.logger.debug('Feature {} has value {}'.format(feature.value, intvalPtr._obj.value))

    def AT_GetIntMax(self, hndl, feature, intmaxptr):
        andorDll.AT_GetIntMax.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_64))
        andorDll.AT_GetIntMax.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetIntMax(hndl, feature, intmaxptr))
        try:
            self.logger.debug('Feature {} has max value {}'.format(feature.value, intmaxptr.value))
        except AttributeError:
            self.logger.debug('Feature {} has max value {}'.format(feature.value, intmaxptr._obj.value))

    def AT_GetIntMin(self, hndl, feature, intminptr):
        andorDll.AT_GetIntMin.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_64))
        andorDll.AT_GetIntMin.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetIntMin(hndl, feature, intminptr))
        try:
            self.logger.debug('Feature {} has min value {}'.format(feature.value, intminptr.value))
        except AttributeError:
            self.logger.debug('Feature {} has min value {}'.format(feature.value, intminptr._obj.value))

    def AT_SetFloat(self, hndl, feature, floatval):
        andorDll.AT_SetFloat.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.c_double)
        andorDll.AT_SetFloat.restype = ctypes.c_int
        self._errcheck(andorDll.AT_SetFloat(hndl, feature, floatval))
        self.logger.debug('Feature {} is set to {}'.format(feature.value, floatval.value))

    def AT_GetFloat(self, hndl, feature, floatval):
        andorDll.AT_GetFloat.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(ctypes.c_double))
        andorDll.AT_GetFloat.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetFloat(hndl, feature, floatval))
        try:
            self.logger.debug('Feature {} has value {}'.format(feature.value, floatval.value))
        except AttributeError:
            self.logger.debug('Feature {} has value {}'.format(feature.value, floatval._obj.value))

    def AT_GetFloatMax(self, hndl, feature, floatmax):
        andorDll.AT_GetFloatMax.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(ctypes.c_double))
        andorDll.AT_GetFloatMax.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetFloatMax(hndl, feature, floatmax))
        try:
            self.logger.debug('Feature {} has max value {}'.format(feature.value, floatmax.value))
        except AttributeError:
            self.logger.debug('Feature {} has max value {}'.format(feature.value, floatmax._obj.value))

    def AT_GetFloatMin(self, hndl, feature, floatmin):
        andorDll.AT_GetFloatMin.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(ctypes.c_double))
        andorDll.AT_GetFloatMin.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetFloatMin(hndl, feature, floatmin))
        try:
            self.logger.debug('Feature {} has min value {}'.format(feature.value, floatmin.value))
        except AttributeError:
            self.logger.debug('Feature {} has min value {}'.format(feature.value, floatmin._obj.value))

    def AT_SetBool(self, hndl, feature, boolval):
        andorDll.AT_SetBool.argtypes = (self.AT_H, self.AT_WCPtr, self.AT_BOOL)
        andorDll.AT_SetBool.restype = ctypes.c_int
        self._errcheck(andorDll.AT_SetBool(hndl, feature, boolval))
        self.logger.debug('Feature {} is set to boolean {}'.format(feature.value, bool(boolval.value)))

    def AT_GetBool(self, hndl, feature, boolval):
        andorDll.AT_GetBool.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_BOOL))
        andorDll.AT_GetBool.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetBool(hndl, feature, boolval))
        try:
            self.logger.debug('Feature {} is of boolean {}'.format(feature.value, bool(boolval.value)))
        except AttributeError:
            self.logger.debug('Feature {} is of boolean {}'.format(feature.value, bool(boolval._obj.value)))

    def AT_SetEnumIndex(self, hndl, feature, enumindex):
        andorDll.AT_SetEnumIndex.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.c_int)
        andorDll.AT_SetEnumIndex.restype = ctypes.c_int
        self._errcheck(andorDll.AT_SetEnumIndex(hndl, feature, enumindex))
        self.logger.debug('Feature {} is set to index {}'.format(feature.value, enumindex.value))

    def AT_SetEnumString(self, hndl, feature, enumstring):
        andorDll.AT_SetEnumString.argtypes = (self.AT_H, self.AT_WCPtr, self.AT_WCPtr)
        andorDll.AT_SetEnumString.restype = ctypes.c_int
        self._errcheck(andorDll.AT_SetEnumString(hndl, feature, enumstring))
        self.logger.debug('Feature {} is set to {}'.format(feature.value, enumstring.value))

    def AT_GetEnumIndex(self, hndl, feature, enumindex):
        andorDll.AT_GetEnumIndex.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(ctypes.c_int))
        andorDll.AT_GetEnumIndex.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetEnumIndex(hndl, feature, enumindex))
        try:
            self.logger.debug('Feature {} has index value {}'.format(feature.value, enumindex.value))
        except AttributeError:
            self.logger.debug('Feature {} has index value {}'.format(feature.value, enumindex._obj.value))

    def AT_GetEnumCount(self, hndl, feature, enumcount):
        andorDll.AT_GetEnumCount.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(ctypes.c_int))
        andorDll.AT_GetEnumCount.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetEnumCount(hndl, feature, ctypes.byref(enumcount)))
        try:
            self.logger.debug('Feature {} has index count {}'.format(feature.value, enumcount.value))
        except AttributeError:
            self.logger.debug('Feature {} has index count {}'.format(feature.value, enumcount._obj.value))

    def AT_IsEnumIndexAvailable(self, hndl, feature, enumindex, available):
        andorDll.AT_IsEnumIndexAvailable.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.c_int,
                                                     ctypes.POINTER(self.AT_BOOL))
        andorDll.AT_IsEnumIndexAvailable.restype = ctypes.c_int
        self._errcheck(andorDll.AT_IsEnumIndexAvailable(hndl, feature, enumindex, ctypes.byref(available)))
        try:
            self.logger.debug('Feature {} is available: {}'.format(feature.value, bool(available.value)))
        except AttributeError:
            self.logger.debug('Feature {} is available: {}'.format(feature.value, bool(available._obj.value)))

    def AT_IsEnumIndexImplemented(self, hndl, feature, enumindex, implemented):
        andorDll.AT_IsEnumIndexImplemented.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.c_int,
                                                       ctypes.POINTER(self.AT_BOOL))
        andorDll.AT_IsEnumIndexImplemented.restype = ctypes.c_int
        self._errcheck(andorDll.AT_IsEnumIndexImplemented(hndl, feature, enumindex, ctypes.byref(implemented)))
        try:
            self.logger.debug('Feature {} is implemented: {}'.format(feature.value, bool(implemented.value)))
        except AttributeError:
            self.logger.debug('Feature {} is implemented: {}'.format(feature.value, bool(implemented._obj.value)))

    def AT_GetEnumStringByIndex(self, hndl, feature, enumindex, strg, strglen):
        andorDll.AT_GetEnumStringByIndex.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.c_int, self.AT_WCPtr,
                                                     ctypes.c_int)
        andorDll.AT_GetEnumStringByIndex.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetEnumStringByIndex(hndl, feature, enumindex, strg, strglen))
        self.logger.debug('Feature {} descriptor received'.format(feature.value))

    def AT_Command(self, hndl, feature):
        andorDll.AT_Command.argtypes = (self.AT_H, self.AT_WCPtr)
        andorDll.AT_Command.restype = ctypes.c_int
        self._errcheck(andorDll.AT_Command(hndl, feature))
        self.logger.debug('Excecuting command {}'.format(feature.value))

    def AT_SetString(self, hndl, feature, strval):
        andorDll.AT_SetString.argtypes = (self.AT_H, self.AT_WCPtr, self.AT_WCPtr)
        andorDll.AT_SetString.restype = ctypes.c_int
        self._errcheck(andorDll.AT_SetString(hndl, feature, strval))
        self.logger.debug('Feature {} set to {}'.format(feature.value, strval.value))

    def AT_GetString(self, hndl, feature, strval, strlen):
        andorDll.AT_GetString.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(self.AT_WC), ctypes.c_int)
        andorDll.AT_GetString.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetString(hndl, feature, strval, strlen))
        self.logger.debug('Feature {} descriptor received'.format(feature.value))

    def AT_GetStringMaxLength(self, hndl, feature, maxstringlength):
        andorDll.AT_GetStringMaxLength.argtypes = (self.AT_H, self.AT_WCPtr, ctypes.POINTER(ctypes.c_int))
        andorDll.AT_GetStringMaxLength.restype = ctypes.c_int
        self._errcheck(andorDll.AT_GetStringMaxLength(hndl, feature, ctypes.byref(maxstringlength)))
        try:
            self.logger.debug('Maximal string length of feature {} is {}'.format(feature.value, maxstringlength.value))
        except AttributeError:
            self.logger.debug('Maximal string length of feature {} is {}'.format(feature.value,
                                                                                 maxstringlength._obj.value))

    def AT_QueueBuffer(self, hndl, ptr, ptrsize):
        andorDll.AT_QueueBuffer.argtypes = (self.AT_H, ctypes.POINTER(self.AT_U8), ctypes.c_int)
        andorDll.AT_QueueBuffer.restype = ctypes.c_int
        self._errcheck(andorDll.AT_QueueBuffer(hndl, ptr, ptrsize))
        self.logger.debug('Buffer {} queued'.format(ptr.contents.value))

    def AT_WaitBuffer(self, hndl, ptrptr, ptrsize, timeout):
        andorDll.AT_WaitBuffer.argtypes = (self.AT_H, ctypes.POINTER(ctypes.POINTER(self.AT_U8)),
                                           ctypes.POINTER(ctypes.c_int), ctypes.c_uint)
        andorDll.AT_WaitBuffer.restype = ctypes.c_int
        self._errcheck(andorDll.AT_WaitBuffer(hndl, ptrptr, ptrsize, timeout))
        self.logger.debug('Waiting for buffer {}'.format(ptrptr._obj.contents.value))

    def AT_Flush(self, hndl):
        andorDll.AT_Flush.argtypes = (self.AT_H,)
        andorDll.AT_Flush.restype = ctypes.c_int
        self._errcheck(andorDll.AT_Flush(hndl))
        self.logger.debug('Buffer queue flushed')

    # ATUtility Library
    def AT_InitialiseUtilityLibrary(self):
        andorUtility.AT_InitialiseUtilityLibrary.argtypes = ()
        andorUtility.AT_InitialiseUtilityLibrary.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_InitialiseUtilityLibrary())
        self.logger.debug('Utility library initialised')

    def AT_FinaliseUtilityLibrary(self):
        andorUtility.AT_FinaliseUtilityLibrary.argtypes = ()
        andorUtility.AT_FinaliseUtilityLibrary.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_FinaliseUtilityLibrary())
        self.logger.debug('Utility library finalised')

    def AT_ConvertBuffer(self, inputbuffer, outputbuffer, width, height, stride, inputpixelenconding,
                         outputpixelencoding):
        andorUtility.AT_ConvertBuffer.argtypes = (ctypes.POINTER(self.AT_U8), ctypes.POINTER(self.AT_U8), self.AT_64,
                                                  self.AT_64, self.AT_64, self.AT_WCPtr, self.AT_WCPtr)
        andorUtility.AT_ConvertBuffer.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_ConvertBuffer(inputbuffer, outputbuffer, width, height, stride,
                                                       inputpixelenconding, outputpixelencoding))
        self.logger.debug('Buffer converted from {} to {}'.format(inputpixelenconding.value, outputpixelencoding.value))

    def AT_ConvertBufferUsingMetadata(self, inputbuffer, outputbuffer, imagesizebyte, outputpixelencoding):
        andorUtility.AT_ConvertBufferUsingMetadata.argtypes = (ctypes.POINTER(self.AT_U8), ctypes.POINTER(self.AT_U8),
                                                               self.AT_64, self.AT_WCPtr)
        andorUtility.AT_ConvertBufferUsingMetadata.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_ConvertBufferUsingMetadata(inputbuffer, outputbuffer, imagesizebyte,
                                                                    outputpixelencoding))
        self.logger.debug('Buffer converted with metadata to {}'.format(outputpixelencoding.value))

    def AT_GetWidthFromMetadata(self, inputbuffer, imagesizebyte, width):
        andorUtility.AT_GetWidthFromMetadata.argtypes = (ctypes.POINTER(self.AT_U8), self.AT_64,
                                                         ctypes.POINTER(self.AT_64))
        andorUtility.AT_GetWidthFromMetadata.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_GetWidthFromMetadata(inputbuffer, imagesizebyte, width))
        try:
            self.logger.debug('Buffer width in metadata is {}'.format(width.value))
        except AttributeError:
            self.logger.debug('Buffer width in metadata is {}'.format(width._obj.value))

    def AT_GetHeightFromMetadata(self, inputbuffer, imagesizebyte, height):
        andorUtility.AT_GetHeightFromMetadata.argtypes = (ctypes.POINTER(self.AT_U8), self.AT_64,
                                                          ctypes.POINTER(self.AT_64))
        andorUtility.AT_GetHeightFromMetadata.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_GetHeightFromMetadata(inputbuffer, imagesizebyte, height))
        try:
            self.logger.debug('Buffer height in metadata is {}'.format(height.value))
        except AttributeError:
            self.logger.debug('Buffer height in metadata is {}'.format(height._obj.value))

    def AT_GetStrideFromMetadata(self, inputbuffer, imagesizebyte, stride):
        andorUtility.AT_GetStrideFromMetadata.argtypes = (ctypes.POINTER(self.AT_U8), self.AT_64,
                                                          ctypes.POINTER(self.AT_64))
        andorUtility.AT_GetStrideFromMetadata.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_GetStrideFromMetadata(inputbuffer, imagesizebyte, stride))
        try:
            self.logger.debug('Buffer stride in metadata is {}'.format(stride.value))
        except AttributeError:
            self.logger.debug('Buffer stride in metadata is {}'.format(stride._obj.value))

    def AT_GetPixelEncodingFromMetadata(self, inputbuffer, imagesizebyte, pixelencoding, pixelencodingsize):
        andorUtility.AT_GetPixelEncodingFromMetadata.argtypes = (ctypes.POINTER(self.AT_U8), self.AT_64,
                                                                 ctypes.POINTER(self.AT_WC), self.AT_64)
        andorUtility.AT_GetPixelEncodingFromMetadata.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_GetPixelEncodingFromMetadata(inputbuffer, imagesizebyte, pixelencoding,
                                                                      pixelencodingsize))
        self.logger.debug('Buffer pixelencoding in metadata received')

    def AT_GetTimeStampFromMetadata(self, inputbuffer, imagesizebyte, stride):
        andorUtility.AT_GetTimeStampFromMetadata.argtypes = (ctypes.POINTER(self.AT_U8), self.AT_64,
                                                             ctypes.POINTER(self.AT_64))
        andorUtility.AT_GetTimeStampFromMetadata.restype = ctypes.c_int
        self._errcheckut(andorUtility.AT_GetTimeStampFromMetadata(inputbuffer, imagesizebyte, stride))
        try:
            self.logger.debug('Buffer timestamp in metadata is {}'.format(stride.value))
        except AttributeError:
            self.logger.debug('Buffer timestamp in metadata is {}'.format(stride._obj.value))
