from pyandorsdk3.base import Andorbase
import ctypes
import numpy as np
import warnings
from collections import deque
import sys
import time


class Istarbuffer(object):
    def __init__(self):
        self.buffer = None
        self.bufferu8 = None
        self.buffersizebytes = None
        self.metadata = {'width': None, 'height': None, 'stride': None, 'inputpixelencoding': None, 'timestamp': None}
        self.metadataenable = None


class Istarscmos(Andorbase):
    """
    Class object for pythonic control of iStar sCMOS
    """

    def __init__(self, camnumber=0):
        super().__init__()
        self._featuretype = {
            'AccumulateCount': 'int',
            'AOIBinning': 'enum',
            'AOIHBin': 'int',
            'AOIHeight': 'int',
            'AOILayout': 'enum',
            'AOILeft': 'int',
            'AOIStride': 'int',
            'AOITop': 'int',
            'AOIVBin': 'int',
            'AOIWidth': 'int',
            'AuxOutSourceTwo': 'enum',
            'Baseline': 'int',
            'BitDepth': 'enum',
            'BufferOverflowEvent': 'int',
            'BytesPerPixel': 'float',
            'CameraAcquiring': 'bool',
            'CameraModel': 'string',
            'CameraName': 'string',
            'CameraPresent': 'bool',
            'CycleMode': 'enum',
            'DDGIOCEnable': 'bool',
            'DDGIOCNumberOfPulses': 'int',
            'DDGIOCPeriod': 'int',
            'DDGOutputDelay': 'int',
            'DDGOutputEnable': 'bool',
            'DDGOutputStepEnable': 'bool',
            'DDGOpticalWidthEnable': 'bool',
            'DDGOutputPolarity': 'enum',
            'DDGOutputSelector': 'enum',
            'DDGOutputWidth': 'int',
            'DDGStepCount': 'int',
            'DDGStepDelayCoefficientA': 'float',
            'DDGStepDelayCoefficientB': 'float',
            'DDGStepDelayMode': 'enum',
            'DDGStepEnabled': 'bool',
            'DDGStepUploadProgress': 'int',
            'DDGStepUploadRequired': 'bool',
            'DDGStepWidthCoefficientA': 'float',
            'DDGStepWidthCoefficientB': 'float',
            'DDGStepWidthMode': 'enum',
            'DeviceCount': 'int',  # System Level
            'ElectronicShutteringMode': 'enum',
            'EventEnable': 'bool',
            'EventsMissedEvent': 'int',
            'EventSelector': 'enum',
            'ExposedPixelHeight': 'int',
            'ExposureTime': 'float',
            'ExposureEndEvent': 'int',
            'ExposureStartEvent': 'int',
            'ExternalTriggerDelay': 'float',
            'FanSpeed': 'enum',
            'FastAOIFrameRateEnable': 'bool',
            'FirmwareVersion': 'string',
            'FrameCount': 'int',
            'FrameRate': 'float',
            'FullAOIControl': 'bool',
            'GateMode': 'enum',
            'I2CAddress': 'int',
            'I2CByte': 'int',
            'I2CByteCount': 'int',
            'I2CByteSelector': 'int',
            'ImageSizeBytes': 'int',
            'InsertionDelay': 'enum',
            'InterfaceType': 'string',
            'IOInvert': 'bool',
            'IOSelector': 'enum',
            'LogLevel': 'enum',  # System Level
            'MaxInterfaceTransferRate': 'float',
            'MCPGain': 'int',
            'MCPIntelligate': 'bool',
            'MCPVoltage': 'int',
            'MetadataEnable': 'bool',
            'MetadataFrame': 'bool',
            'MetadataTimestamp': 'bool',
            'MultitrackBinned': 'bool',
            'MultitrackCount': 'int',
            'MultitrackEnd': 'int',
            'MultitrackSelector': 'int',
            'MultitrackStart': 'int',
            'Overlap': 'bool',
            'PIVEnable': 'bool',
            'PixelEncoding': 'enum',
            'PixelHeight': 'float',
            'PixelReadoutRate': 'enum',
            'PixelWidth': 'float',
            'PreTriggerEnable': 'bool',
            'ReadoutTime': 'float',
            'SensorCooling': 'bool',
            'SensorHeight': 'int',
            'SensorTemperature': 'float',
            'SensorWidth': 'int',
            'SerialNumber': 'string',
            'ShutterMode': 'enum',
            'ShutterTransferTime': 'float',
            'SimplePreAmpGainControl': 'enum',
            'SoftwareVersion': 'string',  # System Level
            'SpuriousNoiseFilter': 'bool',
            'StaticBlemishCorrection': 'bool',
            'TemperatureStatus': 'enum',
            'TimestampClock': 'int',
            'TimestampClockFrequency': 'int',
            'TriggerMode': 'enum',
            'VerticallyCentreAOI': 'bool'
        }
        self._featureval = {key: None for key in self._featuretype.keys()}
        self.camnumber = camnumber
        self.handle = self.AT_H()

    def __enter__(self):
        self.AT_InitialiseLibrary()
        self.AT_InitialiseUtilityLibrary()
        self.open()
        self.getallfeatures()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.flush()
        self.close()
        self.AT_FinaliseUtilityLibrary()
        self.AT_FinaliseLibrary()

    def open(self):
        self.AT_Open(self.camnumber, ctypes.byref(self.handle))
        self.logger.info('Camera with handle {} opened.'.format(self.handle.value))

    def close(self):
        self.AT_Close(self.handle)
        self.logger.info('Camera with handle {} closed.'.format(self.handle.value))

    def getfeature(self, feature):
        featuretype = self._featuretype[feature]
        featurestr = self.AT_WCPtr(feature)
        if featuretype == 'int':
            intval = self.AT_64()
            if feature == 'DeviceCount':
                self.AT_GetInt(self.AT_HANDLE_SYSTEM, featurestr, ctypes.byref(intval))
            else:
                self.AT_GetInt(self.handle, featurestr, ctypes.byref(intval))
            value = intval.value
        elif featuretype == 'enum':
            # TODO create buffer according to max length
            enumval = ctypes.create_unicode_buffer(40)
            enumvalptr = ctypes.cast(enumval, ctypes.POINTER(self.AT_WC))
            enumindex = ctypes.c_int()
            if feature == 'LogLevel':
                self.AT_GetEnumIndex(self.AT_HANDLE_SYSTEM, featurestr, ctypes.byref(enumindex))
                self.AT_GetEnumStringByIndex(self.AT_HANDLE_SYSTEM, featurestr, enumindex, enumvalptr, 40)
            else:
                self.AT_GetEnumIndex(self.handle, featurestr, ctypes.byref(enumindex))
                self.AT_GetEnumStringByIndex(self.handle, featurestr, enumindex, enumvalptr, 40)
            value = enumval.value
        elif featuretype == 'bool':
            boolval = self.AT_BOOL()
            self.AT_GetBool(self.handle, featurestr, ctypes.byref(boolval))
            value = bool(boolval.value)
        elif featuretype == 'float':
            floatval = ctypes.c_double()
            self.AT_GetFloat(self.handle, featurestr, ctypes.byref(floatval))
            value = floatval.value
        elif featuretype == 'string':
            # TODO create buffer according to max length
            strval = ctypes.create_unicode_buffer(40)
            strvalptr = ctypes.cast(strval, ctypes.POINTER(self.AT_WC))
            if feature == 'SoftwareVersion':
                self.AT_GetString(self.AT_HANDLE_SYSTEM, featurestr, strvalptr, 40)
            else:
                self.AT_GetString(self.handle, featurestr, strvalptr, 40)
            value = strval.value
        else:
            raise TypeError('Feature Type unknown')
        self._featureval[feature] = value
        return value

    def getfeaturemin(self, feature):
        featuretype = self._featuretype[feature]
        featurestr = self.AT_WCPtr(feature)
        if featuretype == 'int':
            intval = self.AT_64()
            if feature == 'DeviceCount':
                self.AT_GetIntMin(self.AT_HANDLE_SYSTEM, featurestr, ctypes.byref(intval))
            else:
                self.AT_GetIntMin(self.handle, featurestr, ctypes.byref(intval))
            value = intval.value
        elif featuretype == 'enum':
            self.logger.error("Enum does not have min value.")
            raise ValueError("Enum does not have min value.")
        elif featuretype == 'bool':
            self.logger.error("Bool does not have min value.")
            raise ValueError("Bool does not have min value.")
        elif featuretype == 'float':
            floatval = ctypes.c_double()
            self.AT_GetFloatMin(self.handle, featurestr, ctypes.byref(floatval))
            value = floatval.value
        elif featuretype == 'string':
            self.logger.error("String does not have min value.")
            raise ValueError("String does not have min value.")
        else:
            raise TypeError('Feature Type unknown')
        return value

    def getfeaturemax(self, feature):
        featuretype = self._featuretype[feature]
        featurestr = self.AT_WCPtr(feature)
        if featuretype == 'int':
            intval = self.AT_64()
            if feature == 'DeviceCount':
                self.AT_GetIntMax(self.AT_HANDLE_SYSTEM, featurestr, ctypes.byref(intval))
            else:
                self.AT_GetIntMax(self.handle, featurestr, ctypes.byref(intval))
            value = intval.value
        elif featuretype == 'enum':
            self.logger.error("Enum does not have min value.")
            raise ValueError("Enum does not have min value.")
        elif featuretype == 'bool':
            self.logger.error("Bool does not have min value.")
            raise ValueError("Bool does not have min value.")
        elif featuretype == 'float':
            floatval = ctypes.c_double()
            self.AT_GetFloatMax(self.handle, featurestr, ctypes.byref(floatval))
            value = floatval.value
        elif featuretype == 'string':
            self.logger.error("String does not have min value.")
            raise ValueError("String does not have min value.")
        else:
            raise TypeError('Feature Type unknown')
        return value

    def setfeature(self, feature, featvalue, checkwrite=False):
        featuretype = self._featuretype[feature]
        featurestr = self.AT_WCPtr(feature)
        if featuretype == 'int':
            if type(featvalue) != int:
                raise TypeError('Value must be of type int')
            intval = self.AT_64(featvalue)
            self.AT_SetInt(self.handle, featurestr, intval)
        elif featuretype == 'enum':
            if type(featvalue) != str:
                raise TypeError('Value must be of type str')
            enumvalptr = self.AT_WCPtr(featvalue)
            # Workaround for Exposure time bug
            # see #16 https://gitlab.ethz.ch/ifd-lab/device-driver/pyandorsdk3/issues/16
            if feature == 'TriggerMode':
                exptime = self.getfeature('ExposureTime')
                if exptime < 0.012:
                    self.logger.error("Exposure time must be at least 0.012 before set to External TriggerMode")
                    raise ValueError("Exposure time must be at least 0.012 before set to External TriggerMode")
            if feature == 'LogLevel':
                self.AT_SetEnumString(self.AT_HANDLE_SYSTEM, featurestr, enumvalptr)
            else:
                self.AT_SetEnumString(self.handle, featurestr, enumvalptr)
        elif featuretype == 'bool':
            if type(featvalue) != bool:
                raise TypeError('Value must be of type bool')
            boolval = self.AT_BOOL(int(featvalue))
            self.AT_SetBool(self.handle, featurestr, boolval)
        elif featuretype == 'float':
            if (type(featvalue) == float) and (type(featvalue) == int):
                raise TypeError('Value must be of type float or int')
            featvalue = float(featvalue)
            floatval = ctypes.c_double(featvalue)
            self.AT_SetFloat(self.handle, featurestr, floatval)
        elif featuretype == 'string':
            if type(featvalue) != str:
                raise TypeError('Value must be of type str')
            strval = self.AT_WCPtr(featvalue)
            self.AT_SetString(self.handle, featurestr, strval)
        else:
            raise TypeError('Feature Type unknown')
        if checkwrite:
            checkvalue = self.getfeature(feature)
            if checkvalue != featvalue:
                self.logger.warning('Feature {} has not been set to {}, is {}'.format(feature, featvalue, checkvalue))
                warnings.warn('Feature {} has not been set to {}, is {}'.format(feature, featvalue, checkvalue))
        self.logger.info('Feature {} has  been set to {}'.format(feature, featvalue))
        self._featureval[feature] = featvalue

    def getallfeatures(self):
        [self.getfeature(feat) for feat in self._featureval.keys()
         if not (feat == 'BufferOverflowEvent' or feat == 'EventsMissedEvent' or feat == 'ExposureEndEvent'
                 or feat == 'ExposureStartEvent')]

    def command(self, cmd):
        cmdstr = self.AT_WCPtr(cmd)
        self.AT_Command(self.handle, cmdstr)
        self.logger.info('Command {} issued'.format(cmd))

    def createbuffer(self):
        return Istarbuffer()

    def queuebuffer(self, buf: Istarbuffer):
        if not buf.buffer:
            self.getfeature('ImageSizeBytes')
            buf.buffer = ctypes.create_string_buffer(self._featureval['ImageSizeBytes'])
            buf.bufferu8 = ctypes.cast(buf.buffer, ctypes.POINTER(self.AT_U8))
            buf.buffersizebytes = ctypes.sizeof(buf.buffer)
            buf.metadataenable = self._featureval['MetadataEnable']
            buf.metadata['width'] = self.getfeature('AOIWidth')
            buf.metadata['height'] = self.getfeature('AOIHeight')
            buf.metadata['stride'] = self.getfeature('AOIStride')
            buf.metadata['inputpixelencoding'] = self.getfeature('PixelEncoding')
        self.AT_QueueBuffer(self.handle, buf.bufferu8, ctypes.c_int(buf.buffersizebytes))
        self.logger.info('Buffer at address {} queued'.format(ctypes.addressof(buf.buffer)))

    def waitbuffer(self, buf, timeout=1000):
        self.AT_WaitBuffer(self.handle, ctypes.byref(buf.bufferu8), ctypes.c_int(buf.buffersizebytes),
                           ctypes.c_uint(timeout))

    def convertbuffer(self, buf: Istarbuffer, outputpixelencoding=None):
        if not outputpixelencoding:
            outputpixelencoding = 'Mono16'
        if buf.metadataenable:
            width = self.AT_64()
            height = self.AT_64()
            stride = self.AT_64()
            timestamp = self.AT_64()
            inputpixelencoding = ctypes.create_unicode_buffer(40)
            inputpixelencodingptr = ctypes.cast(inputpixelencoding, ctypes.POINTER(self.AT_WC))
            self.AT_GetWidthFromMetadata(buf.bufferu8, self.AT_64(buf.buffersizebytes), ctypes.byref(width))
            self.AT_GetHeightFromMetadata(buf.bufferu8, self.AT_64(buf.buffersizebytes), ctypes.byref(height))
            self.AT_GetStrideFromMetadata(buf.bufferu8, self.AT_64(buf.buffersizebytes), ctypes.byref(stride))
            self.AT_GetTimeStampFromMetadata(buf.bufferu8, self.AT_64(buf.buffersizebytes), ctypes.byref(timestamp))
            self.AT_GetPixelEncodingFromMetadata(buf.bufferu8, self.AT_64(buf.buffersizebytes),
                                                 inputpixelencodingptr, 40)
            if width.value != buf.metadata['width']:
                raise ValueError('Width in metadata is not consistent with buffer')
            if height.value != buf.metadata['height']:
                raise ValueError('Height in metadata is not consistent with buffer')
            if stride.value != buf.metadata['stride']:
                raise ValueError('Stride in metadata is not consistent with buffer')
            if inputpixelencoding.value != buf.metadata['inputpixelencoding']:
                raise ValueError('Inputpixelencoding in metadata is not consistent with buffer')
            buf.metadata['timestamp'] = timestamp.value
        if outputpixelencoding == 'Mono16':
            img = ctypes.create_string_buffer(buf.metadata['height'] * buf.metadata['width']
                                              * ctypes.sizeof(ctypes.c_uint16))
            outputpixelencodingptr = self.AT_WCPtr(outputpixelencoding)
        elif outputpixelencoding == 'Mono32':
            img = ctypes.create_string_buffer(buf.metadata['height'] * buf.metadata['width']
                                              * ctypes.sizeof(ctypes.c_uint32))
            outputpixelencodingptr = self.AT_WCPtr(outputpixelencoding)
        else:
            raise ValueError('Unknown output encoding')
        imgu8 = ctypes.cast(img, ctypes.POINTER(self.AT_U8))
        if buf.metadataenable:
            self.AT_ConvertBufferUsingMetadata(buf.bufferu8, imgu8, self.AT_64(buf.buffersizebytes),
                                               outputpixelencodingptr)
        else:
            self.AT_ConvertBuffer(buf.bufferu8, imgu8, self.AT_64(buf.metadata['width']),
                                  self.AT_64(buf.metadata['height']), self.AT_64(buf.metadata['stride']),
                                  self.AT_WCPtr(buf.metadata['inputpixelencoding']), self.AT_WCPtr(outputpixelencoding))
        if outputpixelencoding == 'Mono16':
            imgnp = np.fromstring(img, np.uint16)
        elif outputpixelencoding == 'Mono32':
            imgnp = np.frombuffer(img, np.uint32)
        imgnp = np.reshape(imgnp, (buf.metadata['height'], buf.metadata['width']))
        return np.copy(imgnp), buf.metadata

    def flush(self):
        self.AT_Flush(self.handle)
        self.logger.info('Camera with handle {} flushed.'.format(self.handle.value))

    def isimplemented(self, feature):
        isimplemented = self.AT_BOOL()
        self.AT_IsImplemented(self.handle, self.AT_WCPtr(feature), ctypes.byref(isimplemented))
        return bool(isimplemented)

    def isreadonly(self, feature):
        isreadonly = self.AT_BOOL()
        self.AT_IsReadOnly(self.handle, self.AT_WCPtr(feature), ctypes.byref(isreadonly))
        return bool(isreadonly)

    def iswriteable(self, feature):
        iswriteable = self.AT_BOOL()
        self.AT_IsWriteable(self.handle, self.AT_WCPtr(feature), ctypes.byref(iswriteable))
        return bool(iswriteable)

    def isreadable(self, feature):
        isreadable = self.AT_BOOL()
        self.AT_IsReadable(self.handle, self.AT_WCPtr(feature), ctypes.byref(isreadable))
        return bool(isreadable)

    # Convenience functions not from SDK
    def stabilizecooling(self, waittime=2):
        """ Activates the cooling of the camera and returns on stabilized cooling.

        :param waittime: Time in seconds to wait for next check of temperature status. Default 2
        :return: None
        """
        self.setfeature('SensorCooling', True)
        points = deque(['/', '-', '\\', '.', '/', '-', '\\', '.'])
        print('Waiting for sensor cooling to stabilize...')
        start = time.time()
        while self.getfeature('TemperatureStatus') != "Stabilised":
            sys.stdout.write(
                '\r' + "Waiting with status: " + self.getfeature('TemperatureStatus') + " at "
                + "{:2.2f}".format(self.getfeature('SensorTemperature')) + "C   "
                + points[0] + points[1] + points[2] + points[3]
                + points[4] + points[5] + points[6] + points[7])
            sys.stdout.flush()
            points.rotate(1)
            time.sleep(waittime)
        end = time.time()
        print('\n...cooling stabilised.')
        self.logger.info(
            "Cooling stabilized in{:4.0f} seconds with sensor at {:2.2f}C".format(end - start, self.getfeature(
                'SensorTemperature')))

    def keepcool(self):
        """ Checks for cooling status and blocks further execution until user input.

        :return: None
        """
        if not self.getfeature('SensorCooling'):
            self.setfeature('SensorCooling', True)
        _ = input("Camera is kept cool. Press Enter to continue: ")
