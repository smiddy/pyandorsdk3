from setuptools import setup, find_packages
from distutils.util import convert_path

# Set the current version
main_ns = {}
ver_path = convert_path('pyandorsdk3/version.py')
with open(ver_path) as ver_file:
    exec(ver_file.read(), main_ns)
version = main_ns['__version__']

setup(
    name="pyandorsdk3",
    description="Python API for the Andor SDK 3",
    version=version,
    platforms=["win-amd64"],
    author="Markus J. Schmidt",
    author_email='schmidt@ifd.mavt.ethz.ch',
    license="GNU GPLv3",
    url="https://gitlab.ethz.ch/ifd-lab/device-driver/pyandorsdk3",
    packages=find_packages(),
    install_requires=['numpy'],
    zip_safe=False,
    classifiers=['Development Status :: 4 - Beta',
                 'Environment :: Console',
                 'Intended Audience :: Science/Research',
                 'Intended Audience :: Education',
                 'Topic :: Scientific/Engineering',
                 'Operating System :: Microsoft :: Windows',
                 'License :: OSI Approved :: GNU General Public License v3 '
                 'or later (GPLv3+)',
                 'Programming Language :: Python :: 3.5',
                 'Topic :: Multimedia :: Graphics :: Capture :: Digital Camera',
                 'Topic :: Scientific/Engineering :: Visualization'
                 ]
)
